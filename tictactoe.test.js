const { TicTacToe } = require('./tictactoe');

test('default grid should look like expected', () => {

    const expected = `
|---|---|---|
|---|---|---|
|---|---|---|
`
    const game = new TicTacToe(); 
    const grid = game.getGrid();
    expect(grid).toEqual(expected);

});

test('should be able to play A2 cell', () => {

    const expected = `
|---|-X-|---|
|---|---|---|
|---|---|---|
`
    const game = new TicTacToe(); 
    game.play("A2")
    const grid = game.getGrid();
    expect(grid).toEqual(expected);

});