class TicTacToe {

    grid = {
        "A1": "-",
        "A2": "-",
        "A3": "-",
        "B1": "-",
        "B2": "-",
        "B3": "-",
        "C1": "-",
        "C2": "-",
        "C3": "-",
    }

    play(cell) {
        this.grid[cell] = "X"
    }

    getGrid() {
        return `
|-${this.grid["A1"]}-|-${this.grid["A2"]}-|-${this.grid["A3"]}-|
|-${this.grid["B1"]}-|-${this.grid["B2"]}-|-${this.grid["B3"]}-|
|-${this.grid["C1"]}-|-${this.grid["C2"]}-|-${this.grid["C3"]}-|
`;
    }

}


module.exports = {
    TicTacToe
}